﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FlyDetection : MonoBehaviour {

    public UnityEvent onFlyEnter;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "fly")
        {
            
            onFlyEnter.Invoke(); //invoke toutes les méthodes qui sont en train d'attendre
        }
    }

    // Use this for initialization
    void Start () {

        onFlyEnter.AddListener(debugFlyEnter);
	}
	
	// Update is called once per frame
	void debugFlyEnter () {

        Debug.Log("mouche dans télé");
	}
}
