﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;


[CustomEditor(typeof(RotateWithMouse))]
public class RotateWithMouseEditor : Editor
{
    [MenuItem("Tools / Add / 360 Camera ")]
    public static void AddCamera360InScene()
    {

        GameObject obj = new GameObject("#360Camera");
        RotateWithMouse rotate = obj.AddComponent<RotateWithMouse>();
        rotate.SetRootToRotate(obj.transform);
        obj.transform.parent = null;

        Camera cam = obj.AddComponent<Camera>();
    }

    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();

        RotateWithMouse myScript = (RotateWithMouse)target;

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Reset to zero"))
        {
            myScript.ResetDirection();
        }
        GUILayout.EndHorizontal();



    }
}
